var topic = "<!DOCTYPE html>\r\n\n<!-- saved from url=(0024)http://docs.autodesk.com -->\r\n<html>\n   <head>\n      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n      <meta name=\"product\" content=\"3DSMAX\">\n      <meta name=\"release\" content=\"2020\">\n      <meta name=\"book\" content=\"Customization\">\n      <meta name=\"created\" content=\"2019-06-11\">\n      <meta name=\"topicid\" content=\"GUID-E1A680B8-4352-42AF-8A93-B2C95A543075\">\n      <meta name=\"topic-type\" content=\"concept\">\n      <title>Render Elements and Bake Elements - Quick Navigation</title><script language=\"javascript\">var index = \'index.html\';</script></head>\n   <body height=\"100%\">\n      <div class=\"body_content\" id=\"body-content\" dir=\"ltr\"><a name=\"GUID-E1A680B8-4352-42AF-8A93-B2C95A543075\"></a><div class=\"head\">\n            <h1> Render Elements and Bake Elements - Quick Navigation </h1>\n         </div><a name=\"TABLE_0FC39E01BE0940DA8291FAC3535E12DC\"></a><div class=\"table-not-ruled\">\n            <table cellpadding=\"0\" cellspacing=\"0\" class=\"not-ruled\">\n               <colgroup>\n                  <col width=\"3.387533875338754%\">\n                  <col width=\"41.5650406504065%\">\n                  <col width=\"55.04742547425474%\">\n               </colgroup>\n               <tr class=\"not-ruled-odd-row\">\n                  <td class=\"table-body\" valign=\"middle\">&nbsp; <img src=\"../images/GUID-D311083C-CA5A-4CA1-9624-E5A11D8AAAE7-low.bmp\"> \n                  </td>\n                  <td class=\"table-body\" colspan=\"2\" valign=\"middle\"> <a name=\"P_AA56972734074877961AE6A633727DBC\"></a><p class=\"table-body\">&nbsp;&nbsp;&nbsp; \n                        				  \n                     </p> <a name=\"P_F3954A0AD20A42139650494531159842\"></a><p class=\"table-body\"> <em class=\"strong\"> <span class=\"char_link\"><a href=\"GUID-D986BA05-E8EF-4CD4-8CFC-8325D5D91104.htm\"> Quick Navigation - Main Index \n                                 						</a></span> </em> \n                     </p> \n                  </td>\n               </tr>\n            </table>\n         </div><a name=\"TABLE_9420219EF39F4F2DB7EB2431BEF997F7\"></a><div class=\"table-ruled\">\n            <table cellpadding=\"0\" cellspacing=\"0\" class=\"ruled\">\n               <colgroup>\n                  <col>\n                  <col>\n                  <col>\n               </colgroup>\n               <tr class=\"ruled-odd-row\">\n                  <td class=\"table-body\"> <a name=\"P_B778066427CA404388A6AEB3EF143E2D\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-E8F75D47-B998-4800-A3A5-610E22913CFC.htm\">Interface: RenderElementMgr</a></span> \n                     </p> \n                  </td>\n                  <td class=\"table-body\">&nbsp;</td>\n                  <td class=\"table-body\">&nbsp;</td>\n               </tr>\n               <tr class=\"ruled-even-row\">\n                  <td class=\"table-body\"> <a name=\"P_72C8CB62D8F94817B8642E41E30DB069\"></a><p class=\"table-body\"> <em class=\"strong\"> <span class=\"char_link\"><a href=\"GUID-98D34281-A01F-4495-9246-9CCA14C41AE9.htm\">Render Elements</a></span></em> \n                     </p> \n                  </td>\n                  <td class=\"table-body\">&nbsp;</td>\n                  <td class=\"table-body\">&nbsp;</td>\n               </tr>\n               <tr class=\"ruled-odd-row\">\n                  <td class=\"table-body\"> <a name=\"P_3A8C594B3C674DF3A97EC7BD5D9FCA3F\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-B23A1E49-B5C9-4209-97AC-EF9D7277EFBE.htm\">alphaRenderElement</a></span> \n                     </p> <a name=\"P_04F9407630BF4698B92E07E1A0DFAB06\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-4CCF63F8-9941-4F58-8536-E26F5DFC5BB8.htm\">atmosphereRenderElement</a></span> \n                     </p> <a name=\"P_E13EE400E33D4C6984FA93B203DDA476\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-558E3B02-7DE4-4AC0-9D27-AF11D76B8ED7.htm\">BackgroundRenderElement</a></span> \n                     </p> <a name=\"P_54CA2D44CE974DAFBC7C97C5D720590D\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-22851307-3777-4B95-9E87-308A1E1B3708.htm\">Beauty</a></span> \n                     </p> <a name=\"P_55E66FA2837C44DA90F601EDB97D69C0\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-B936EFD3-23F6-43B6-98B3-EABF193C5B7C.htm\">BlendRenderElement</a></span> \n                     </p> <a name=\"P_F94788CC1EF345FDA038307B4D1DD4B3\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-BE739267-7FBE-44D4-B7ED-887DF810867E.htm\">diffuseRenderElement</a></span> \n                     </p> <a name=\"P_17473848B4D54B78BD694E8E817A4CD5\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-2B91525D-AC46-4252-B775-15A469288D18.htm\">emissionRenderElement</a></span> \n                     </p> <a name=\"P_1F99D514D15546D39CC4005230078013\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-CECC1D60-00F6-4C5F-9395-A0C0B9AF66A8.htm\">Illuminance_Render_Element</a></span> \n                     </p> <a name=\"P_94D77F38F4C347AD8516166B6798EF05\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-EC480C7F-939D-45DE-89D1-5801D033737E.htm\">Illumination_Render_Element</a></span> \n                     </p> <a name=\"P_8D639629FB764D7CAEFDC7C060FC5FB4\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-E276D128-6B38-4C48-9CC3-67A2980B08CC.htm\">Ink</a></span> \n                     </p> \n                  </td>\n                  <td class=\"table-body\"> <a name=\"P_DD9D09B3A3634210AA540EA5BA9BF5AE\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-C7EB2C61-2367-48D0-AD75-BB074D78DAC4.htm\">Lighting</a></span> \n                     </p> <a name=\"P_CA463EF974484C94AC66E88CB2633E64\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-7118FF61-FD1E-4EA1-B02D-225C76A86AA1.htm\">Lighting_Analysis_Data</a></span> \n                     </p> <a name=\"P_43687BFFB71B465CA611D6BC15AB76DE\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-C38A1D82-4D32-428A-8E35-F4D7CC3D3265.htm\">Luminance_Render_Element</a></span> \n                     </p> <a name=\"P_D3D05853164B42FB83758B5814D7B7A4\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-C876DFA7-472F-4B9A-A3F2-C9E38625AE23.htm\">Lumination_Render_Element</a></span> \n                     </p> <a name=\"P_1B42FF31F9D64353AAFE462E25555FA3\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-5E81F4FC-AB32-4B7F-816D-229254D2B18C.htm\">Material_ID</a></span> \n                     </p> <a name=\"P_611E9892C58E40BDA13DAB3091975B41\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-94D58828-6110-4009-889F-E3A4DF81F4E3.htm\">MatteRenderElement</a></span> \n                     </p> \n                  </td>\n                  <td class=\"table-body\"> <a name=\"P_EA6BB4F3A56245AA9C4E46F50F8B13DE\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-7F63E7DC-1EDB-495A-BF92-755DD0BA42F3.htm\">Object_ID</a></span> \n                     </p> <a name=\"P_E971122E6CDC4F379A0D8AC7008F69BF\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-6730A2DD-E92B-40BF-8767-53FF7A0F518B.htm\">PaintRenderElement</a></span> \n                     </p> <a name=\"P_32C59B800EEE4C628AEB4782DE1F7F73\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-7B7CD2B8-A2C8-4430-8B79-65C3250AD160.htm\">reflectionRenderElement</a></span> \n                     </p> <a name=\"P_48AF4EC7FA2B4BF9BD03F1EAA3C8844A\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-82CD87DF-24D7-4E98-8542-8153EFBA89C4.htm\">refractionRenderElemen</a></span> \n                     </p> <a name=\"P_1E56E981C04B4B3F8FB56CAAD2657E97\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-FA8C194A-F2BD-4244-86A3-C27DFDFB9C36.htm\">Self_Illumination \n                              					 </a></span> \n                     </p> <a name=\"P_8241F32D7FAC40BF97FD8368E82A55CF\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-2B39BDB0-FCC8-4A42-9C82-6852487CBD06.htm\">ShadowRenderElement</a></span> \n                     </p> <a name=\"P_F251AE2EFE0B4D6A90696BE1C7A623A4\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-3D21BAE3-B958-4817-9FB2-CC66903016FD.htm\">specularRenderElement</a></span> \n                     </p> <a name=\"P_A6767ECC5C744E55B1EF1D520DF4CB4C\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-2A8A1184-CE6B-4A89-B2DE-B3A317811212.htm\">velocity</a></span> \n                     </p> <a name=\"P_DDFDC2BA831A404FB5C904D1A969D56E\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-8D4BB647-2CDD-4821-8502-460EAC95063A.htm\">ZRenderElement</a></span> \n                     </p> \n                  </td>\n               </tr>\n               <tr class=\"ruled-even-row\">\n                  <td class=\"table-body\"> <a name=\"P_8BAC1C3A04624CF5A115C93814A47792\"></a><p class=\"table-body\"> <em class=\"strong\"> <span class=\"char_link\"><a href=\"GUID-60C2F166-93C0-4061-B12D-3616AE38B922.htm\">Bake Elements</a></span> </em> \n                     </p> \n                  </td>\n                  <td class=\"table-body\">&nbsp;</td>\n                  <td class=\"table-body\">&nbsp;</td>\n               </tr>\n               <tr class=\"ruled-odd-row\">\n                  <td class=\"table-body\"> <a name=\"P_1B6B5493CC754C8DAF1A5C72EAA319C6\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-6791EBAB-F0AD-49C9-A4AB-466C217733F1.htm\">AlphaMap</a></span> \n                     </p> <a name=\"P_CB81D67073204762AA3E493CBD1A952B\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-F1683751-BC0E-4A2C-A05C-14C0981D56A8.htm\">Ambient_Occlusion</a></span> \n                     </p> <a name=\"P_61FEB1B0D56546118B6E971715B1C3E5\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-B428E5A9-519B-4C03-851D-4E022197AC6A.htm\">BlendMap</a></span> \n                     </p> <a name=\"P_C3A7543A6C6049ADAC340D56E373EF68\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-2ABB9277-540D-4C11-929C-978DC2A3A600.htm\">CompleteMap</a></span> \n                     </p> \n                  </td>\n                  <td class=\"table-body\"> <a name=\"P_175656D74DEA444D87567E22BC9338E5\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-44EE4330-9024-4C6F-A295-92965EE01997.htm\">diffuseMap</a></span> \n                     </p> <a name=\"P_41B4F0748B4445AFB3BE514CBF521043\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-6A970DFC-B687-4F11-A247-AC8F63931A35.htm\">HeightMap</a></span> \n                     </p> <a name=\"P_42E9DDA98A3843C49058742F2644624C\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-ACB3B772-3BD5-46B7-B278-00E2BCF4C342.htm\">LightingMap</a></span> \n                     </p> <a name=\"P_440CCB92D33B471F922AC76C759DC439\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-0DF288E6-3D47-46F2-BEEF-B462A983DEE5.htm\">Missing_Texture_Bake_Element</a></span> \n                     </p> \n                  </td>\n                  <td class=\"table-body\"> <a name=\"P_BD56FFBAD43B44D7B61E9D30F14448E7\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-55D231E2-7070-40FF-A906-D4E2AF59208E.htm\">NormalsMap</a></span> \n                     </p> <a name=\"P_F528656FD047480CA630762CD5B31CEC\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-BC65B699-708B-40B5-8871-10EE15A0C933.htm\">ShadowsMap</a></span> \n                     </p> <a name=\"P_CB6D6AA39D224010AC385E14A85927A3\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-9E669C64-540E-4108-8035-4120FCF9BE1C.htm\">specularMap</a></span> \n                     </p> \n                  </td>\n               </tr>\n               <tr class=\"ruled-even-row\">\n                  <td class=\"table-body\"> <a name=\"P_ECA695870A7645AFA7BA4C637CDC4A7F\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-61344DFE-8DA6-4E19-A184-BFD9ED093EFF.htm\">Render To Texture Using MAXScript</a></span> \n                     </p> \n                  </td>\n                  <td class=\"table-body\">&nbsp;</td>\n                  <td class=\"table-body\">&nbsp;</td>\n               </tr>\n               <tr class=\"ruled-odd-row\">\n                  <td class=\"table-body\"> <a name=\"P_B615E3D47A1C49869690C95C5E97C80E\"></a><p class=\"table-body\"> <em class=\"strong\"> <span class=\"char_link\"><a href=\"GUID-036D8B7F-3538-4234-B867-E6C50465B1B1.htm\">Exposure Controls (ToneOperators)</a></span> </em> \n                     </p> \n                  </td>\n                  <td class=\"table-body\">&nbsp;</td>\n                  <td class=\"table-body\">&nbsp;</td>\n               </tr>\n               <tr class=\"ruled-even-row\">\n                  <td class=\"table-body\"> <a name=\"P_FD9853FC437345219259D4E29D75B343\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-2853EEBC-2126-4B1F-8254-CC668A7762C3.htm\">Automatic_Exposure_Control</a></span> \n                     </p> <a name=\"P_254A2754766A427C84D956FC0CBCD918\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-70A9861A-5E23-4DD2-8738-C630B5BE2A9C.htm\">Linear_Exposure_Control</a></span> \n                     </p> <a name=\"P_92D21657EC114DEB9BB00F73E66B7720\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-1C0FF8FC-4AB2-43E6-B2D0-EE272C72F27D.htm\">Logarithmic_Exposure_Control \n                              					 </a></span> \n                     </p> \n                  </td>\n                  <td class=\"table-body\"> <a name=\"P_CE4659DA35BF49A2931CADABB763CED1\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"\">mr_Photographic_Exposure_Control</a></span> \n                     </p> <span class=\"char_link\"><a href=\"GUID-F86EB3F0-DF9D-4CD0-8C5C-96E3BDC8444D.htm\">Physical_Camera_Exposure_Control</a></span> \n                  </td>\n                  <td class=\"table-body\"> <a name=\"P_8A9EA9C330AC48258CB12F8EE1FE2827\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-985C3D3A-3D47-4380-8265-38684F71B7FB.htm\">Pseudo_Color_Exposure_Control \n                              					 </a></span> \n                     </p> <a name=\"P_30BE9098E8CE4591861F33877CCF72B6\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-3FB97015-3D67-477E-945A-8B59E5E49F1F.htm\">Interface: SceneExposureControl</a></span> \n                     </p> \n                  </td>\n               </tr>\n            </table>\n         </div>\n         <div class=\"footer-block\"><a href=\"..\" class=\"comments-anchor\" target=\"_blank\"><span class=\"comments-link\">Please send us your comment about this page</span></a></div><br><p class=\"footer-license-block\"><a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\" target=\"_blank\"><img alt=\"Creative Commons License\" style=\"border-width: 0;\" src=\"../images/ccLink.png\"></a>&nbsp;<em>Except where otherwise noted, this work is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\" target=\"_blank\">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a>. Please see the <a href=\"http://autodesk.com/creativecommons\" target=\"_blank\">Autodesk Creative Commons FAQ</a> for more information.</em></p><br></div>\n   </body>\n</html>";