var topic = "<!DOCTYPE html>\r\n\n<!-- saved from url=(0024)http://docs.autodesk.com -->\r\n<html>\n   <head>\n      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n      <meta name=\"product\" content=\"3DSMAX\">\n      <meta name=\"release\" content=\"2020\">\n      <meta name=\"book\" content=\"Customization\">\n      <meta name=\"created\" content=\"2019-06-11\">\n      <meta name=\"topicid\" content=\"GUID-8EC1EEA5-41CD-4E1F-B9FA-562B515EADE7\">\n      <meta name=\"indexterm\" content=\"How To Use DotNet\">\n      <meta name=\"topic-type\" content=\"concept\">\n      <title>How To ... Generate Hash Checksums Using DotNet</title><script language=\"javascript\">var index = \'index.html\';</script><script type=\"text/javascript\" src=\"../scripts/prettify/prettify.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-yaml.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-xq.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-wiki.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-vhdl.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-vb.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-tex.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-sql.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-scala.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-proto.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-n.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-ml.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-lua.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-lisp.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-hs.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-go.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-css.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-clj.js\"></script><script type=\"text/javascript\" src=\"../scripts/prettify/lang-apollo.js\"></script></head>\n   <body height=\"100%\" onLoad=\"javascript:prettyPrint();\">\n      <div class=\"body_content\" id=\"body-content\" dir=\"ltr\"><a name=\"GUID-8EC1EEA5-41CD-4E1F-B9FA-562B515EADE7\"></a><div class=\"head\">\n            <h1>How To ... Generate Hash Checksums Using DotNet</h1>\n         </div><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3357\"></a><div class=\"table-ruled\">\n            <table cellpadding=\"0\" cellspacing=\"0\" class=\"ruled\">\n               <colgroup>\n                  <col width=\"100%\">\n               </colgroup>\n               <tr class=\"ruled-odd-row\">\n                  <td class=\"table-body\"> <a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3358\"></a><p class=\"table-body\"> <span class=\"char_link\"><a href=\"GUID-676FB825-84C1-4708-A398-993266E4D2AD.htm\">How To</a></span> &gt; <span class=\"char_link\"><a href=\"GUID-779FD7AC-953D-4567-B2A8-60B1D8695B95.htm\">Use DotNet</a></span> &gt; Generate Hash Checksums \n                     </p> \n                  </td>\n               </tr>\n            </table>\n         </div>\n         <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC335A\"></a>The following tutorials demonstrates the use of the <em class=\"strong\"> <span class=\"code\">.GetHashCode()</span> </em> method exposed by the <span class=\"code\"> <em class=\"strong\">\"System.String</em> </span>\" dotNetObject for determining whether specific data collected in the form of a string\n            has changed between two runs of the script. \n         </p>\n         <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC335B\"></a> <em class=\"strong\">Related topics:</em> \n         </p>\n         <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC335C\"></a> <span class=\"char_link\"><a href=\"GUID-779FD7AC-953D-4567-B2A8-60B1D8695B95.htm\">DotNet In MAXScript</a></span> \n         </p>\n         <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC335D\"></a> <span class=\"char_link\"><a href=\"GUID-56961A11-BD8A-40E5-BB68-74AA27F2E056.htm\">dotNetObject</a></span> \n         </p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3360\"></a><div class=\"table-ruled\">\n            <table cellpadding=\"0\" cellspacing=\"0\" class=\"ruled\">\n               <colgroup>\n                  <col width=\"100%\">\n               </colgroup>\n               <tr class=\"ruled-odd-row\">\n                  <td class=\"table-body\"> <a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3361\"></a><p class=\"table-body\"> <em class=\"strong\">NATURAL LANGUAGE</em> \n                     </p> \n                  </td>\n               </tr>\n               <tr class=\"ruled-even-row\">\n                  <td class=\"table-body\"> <a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3362\"></a><p class=\"table-body\">Define a MAXScript function to calculate the Hash code of all scene objects. </p> \n                  </td>\n               </tr>\n               <tr class=\"ruled-odd-row\">\n                  <td class=\"table-body\"> <a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3363\"></a><p class=\"table-body\">Collect the name, transformation matrix and parent of every object in a string. </p> \n                  </td>\n               </tr>\n               <tr class=\"ruled-even-row\">\n                  <td class=\"table-body\"> <a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3364\"></a><p class=\"table-body\">Get the HashCode of the string and return it. </p> \n                  </td>\n               </tr>\n            </table>\n         </div><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3367\"></a><div class=\"table-ruled\">\n            <table cellpadding=\"0\" cellspacing=\"0\" class=\"ruled\">\n               <colgroup>\n                  <col width=\"100%\">\n               </colgroup>\n               <tr class=\"ruled-odd-row\">\n                  <td class=\"table-body\"> <a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3368\"></a><p class=\"table-body\"> <em class=\"strong\">EXAMPLE</em> \n                     </p> \n                  </td>\n               </tr>\n               <tr class=\"ruled-even-row\">\n                  <td class=\"table-body\"> \n                     <div class=\"codeBlock\"><code><a name=\"WS73099CC142F487553098682E12AC2FC2BC7-62F6\"></a><pre class=\"prettyprint\">\nfn getHashCodeFromGeometry = (\n  local hashStr = stringstream \"\"\n  for obj in objects do (\n    print obj.name to:hashStr\n    print obj.transform to:hashStr\n    if obj.parent != undefined then print obj.parent.name to:hashStr\n  )--end for loop\n  (dotNetObject \"System.String\" hashStr).GetHashCode()\n)--end function\n</pre></code></div> \n                  </td>\n               </tr>\n            </table>\n         </div>\n         <div class=\"section\"><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3377\"></a> \n            <h2><a name=\"GUID-7B266CB7-B5F4-4DD5-AAB6-3F0E7BD864DC\"></a> <em class=\"strong\">Step-By-Step</em> \n            </h2> \n            <div class=\"codeBlock\"><code><a name=\"WS73099CC142F487553098682E12AC2FC2BC7-62F5\"></a><pre class=\"prettyprint\"><em class=\"codeEmphasisStrong\">fn getHashCodeFromGeometry = (</em></pre></code></div> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3379\"></a>Define a MAXScript function to be called by the Timer. \n            </p> \n            <div class=\"codeBlock\"><code><a name=\"WS73099CC142F487553098682E12AC2FC2BC7-62F4\"></a><pre class=\"prettyprint\"><em class=\"codeEmphasisStrong\">local hashStr = stringstream \"\"</em></pre></code></div> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC337B\"></a>Create an empty string stream to collect the data to convert to Hash Code. \n            </p> \n            <div class=\"codeBlock\"><code><a name=\"WS73099CC142F487553098682E12AC2FC2BC7-62F3\"></a><pre class=\"prettyprint\"><em class=\"codeEmphasisStrong\">for obj in objects do (</em></pre></code></div> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC337D\"></a>Loop through all scene objects... \n            </p> \n            <div class=\"codeBlock\"><code><a name=\"WS73099CC142F487553098682E12AC2FC2BC7-62F2\"></a><pre class=\"prettyprint\"><em class=\"codeEmphasisStrong\">print obj.name to:hashStr</em> <em class=\"codeEmphasisStrong\">print obj.transform to:hashStr</em></pre></code></div> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3380\"></a>And print the name and transform of the current object to the stream. \n            </p> \n            <div class=\"codeBlock\"><code><a name=\"WS73099CC142F487553098682E12AC2FC2BC7-62F1\"></a><pre class=\"prettyprint\"><em class=\"codeEmphasisStrong\">if obj.parent != undefined then print obj.parent.name to:hashStr</em></pre></code></div> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3382\"></a>If the object has a parent, also print the parent\'s name to the stream. \n            </p> \n            <div class=\"codeBlock\"><code><a name=\"WS73099CC142F487553098682E12AC2FC2BC7-62F0\"></a><pre class=\"prettyprint\"><em class=\"codeEmphasisStrong\">)\n(dotNetObject \"System.String\" hashStr).GetHashCode()</em></pre></code></div> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3385\"></a>Finally, create an instance of the System.String class from the collected string stream\n               and get its Hash Code. \n            </p> \n            <div class=\"codeBlock\"><code><a name=\"WS73099CC142F487553098682E12AC2FC2BC7-62EF\"></a><pre class=\"prettyprint\"><em class=\"codeEmphasisStrong\">)--</em>end function</pre></code></div> \n         </div>\n         <div class=\"section\"><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3387\"></a> \n            <h2><a name=\"GUID-C80ECB04-A6E7-4BC4-9ED0-D9FA4D7C17B2\"></a> <em class=\"strong\">Practical Use</em> \n            </h2> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3388\"></a>Let\'s see how this function could be useful in a real-world situation. \n            </p> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3389\"></a>Let\'s assume we are developing a script which has to perform complex time consuming\n               calculations when the scene content has changed. For example, it would have to rebuild\n               all its data when \n            </p> <a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC338A\"></a><ul>\n               <li> \n                  <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC338C\"></a>the object count in the scene has changed (objects were added or removed), \n                  </p> \n               </li>\n               <li> \n                  <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC338E\"></a>one or more objects were renamed \n                  </p> \n               </li>\n               <li> \n                  <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3390\"></a>one or more objects were moved, rotated or scaled \n                  </p> \n               </li>\n               <li> \n                  <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3392\"></a>the hierarchy has changed (objects were linked or unlinked to/from parent objects)\n                     \n                  </p> \n               </li>\n            </ul> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3393\"></a>It would be useful to cache the last results and return the same value next time the\n               script is executed if the scene has not changed since the previous run. If checking\n               the scene for changes is significantly faster than actually processing the data again,\n               this would lead to a nice speed up. \n            </p> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3394\"></a>One possible approach would be to store all this scene state data in some user variables\n               - assign the objects.count value to a variable, collect the names, transformation\n               matrices and parent names of all objects into arrays the first time the script is\n               run and then, the next time the script has to determine whether it needs to recalculate\n               its initial data, it could collect the same information once again and compare all\n               values with the results from the previous run. \n            </p> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3395\"></a>Obviously, if the scene contains 10,000+ objects, the amount of data to store (and\n               the time needed to collect and compare the two data sets) would be quite substantial.\n               \n            </p> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3396\"></a>But since we are not interested in the actual data values but only want to know whether\n               they have changed between runs, we can use the HashCode value which represents ALL\n               the above conditions with a single long integer value. All we have to do is run the\n               function once to get a HashCode number reflecting the count, names, transforms and\n               parents of all scene objects, then if we want to know if any of these properties have\n               changed since the last time we run our script, we can call the function again and\n               compare the two values. If the numbers are the same, the scene has not changed. In\n               that case we can skip any data rebuilding steps since we can be confident the scene\n               is in the same state as before. If the hash values are different, we must reprocess\n               the scene to collect the actual data. \n            </p> \n            <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC3397\"></a>Of course, the above function is just one example of possible HashCode usage. It can\n               be changed to reflect more or less properties of objects, or could be used to track\n               the state of other values that can be represented as strings. \n            </p> \n         </div>\n         <div class=\"see-also\">\n            <h4>See Also</h4>\n            <div class=\"related-links\">\n               <ul>\n                  <li> \n                     <p><a name=\"WS73099CC142F48755-2231E4B3128E0E4A0FC339A\"></a> <span class=\"char_link\"><a href=\"GUID-676FB825-84C1-4708-A398-993266E4D2AD.htm\">\"How To\" Tutorials Index Page</a></span> \n                     </p> \n                  </li>\n               </ul>\n            </div>\n         </div>\n         <div class=\"footer-block\"><a href=\"..\" class=\"comments-anchor\" target=\"_blank\"><span class=\"comments-link\">Please send us your comment about this page</span></a></div><br><p class=\"footer-license-block\"><a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\" target=\"_blank\"><img alt=\"Creative Commons License\" style=\"border-width: 0;\" src=\"../images/ccLink.png\"></a>&nbsp;<em>Except where otherwise noted, this work is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\" target=\"_blank\">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a>. Please see the <a href=\"http://autodesk.com/creativecommons\" target=\"_blank\">Autodesk Creative Commons FAQ</a> for more information.</em></p><br></div>\n   </body>\n</html>";